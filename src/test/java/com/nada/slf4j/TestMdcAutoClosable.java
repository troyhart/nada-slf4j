package com.nada.slf4j;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TestMdcAutoClosable
{

  @Test(expected = NullPointerException.class)
  // cannot create instance with null map
  public void test_nullMap()
  {
    try (MdcAutoClosable mdcac = new MdcAutoClosable(null)) {
      fail("Expecting an exception--creating a new MdcAutoClosable with a null argument.");
    }
  }

  @Test()
  // be sure an exception isn't raised when asked to remove an unknown entry
  public void test_removeUnknown()
  {
    try (MdcAutoClosable mdc = new MdcAutoClosable()) {
      mdc.remove("foo");
    }
  }

  @Test
  // ensure normal removal works as expected
  public void test_remove()
  {
    try (MdcAutoClosable mdc = new MdcAutoClosable()) {
      String key = "foo";
      mdc.put(key, "foovalue");
      mdc.remove(key);
      mdc.remove("bar"); // able to remove un-put key w/o issue
    }
  }

  @Test
  // verify that close can be called more than once without issue.
  public void test_multipleClose()
  {
    try (MdcAutoClosable mdc = new MdcAutoClosable(new HashMap<String, String>())) {
      //closing here, and then it will be auto closed at the termination of this block.
      mdc.close();
    }
  }

  @Test
  // verify closing a non-empty mdc doesn't create an issue.
  public void test_closeNonEmptyMap()
  {
    Map<String, String> context = new HashMap<String, String>();
    context.put("foo", "foovalue");
    context.put("bar", "barvalue");
    context.put("cat", "catvalue");
    
    try (MdcAutoClosable mdc = new MdcAutoClosable(context)) {
      // don't need to do anything here...
      // Closing is automatic after all.
    }
  }
}
