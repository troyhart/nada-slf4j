
package com.nada.slf4j.diagnostics;

import java.util.Random;

/**
 * @author troy.hart@gmail.com
 *
 */
public class ExceptionGenerator
{
  private static Random RANDOM = new Random();

  /**
   * @return randomly one of 4 Exceptions.
   */
  public static Exception randomException()
  {
    switch (RANDOM.nextInt(2)) {
      case 1 :
        return new Exception("case 1");
      case 2 :
        return new Exception("case 2");
      case 3 :
        return new Exception("case 3");
      default :
        return new Exception("case 4");
    }
  }

  /**
   * @return randomly one of 4 RuntimeExceptions.
   */
  public static RuntimeException randomRuntimeException()
  {
    switch (RANDOM.nextInt(2)) {
      case 1 :
        return new RuntimeException("case 1");
      case 2 :
        return new RuntimeException("case 2");
      case 3 :
        return new RuntimeException("case 3");
      default :
        return new RuntimeException("case 4");
    }
  }
}
