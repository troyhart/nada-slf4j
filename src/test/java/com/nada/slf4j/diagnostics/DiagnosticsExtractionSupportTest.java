package com.nada.slf4j.diagnostics;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

/**
 * @author troy.hart@gmail.com
 *
 */
public class DiagnosticsExtractionSupportTest
{
  private static final String VALUE_TWO = "FOO2";

  private static final String KEY_TWO = "FooDiagnosticKey2";

  private static final String VALUE_ONE = "FOO1";

  private static final String KEY_ONE = "FooDiagnosticKey1";

  private static final String VALUE_ZERO = "FOO0";

  private static final String KEY_ZERO = "FooDiagnosticKey0";

  @Test
  public void testDiagnosticsAtRootOnly()
  {
    DiagnosticException e = new DiagnosticException().put(KEY_ZERO, VALUE_ZERO);
    Map<Integer, Map<String, String>> dm = DiagnosticsExtractionSupport.extractDiagnostics(e);
    assertEquals(1, dm.size());
    assertEquals(1, dm.get(0).size());
    assertEquals(KEY_ZERO, dm.get(0).keySet().iterator().next());
    assertEquals(VALUE_ZERO, dm.get(0).values().iterator().next());
  }

  @Test
  public void testDiagnosticsWithMultipleDiagnosticsAvailableCauses()
  {
    DiagnosticException e2 = new DiagnosticException().put(KEY_TWO, VALUE_TWO);
    DiagnosticException e1 = new DiagnosticException(e2).put(KEY_ONE, VALUE_ONE);
    DiagnosticException e0 = new DiagnosticException(e1).put(KEY_ZERO, VALUE_ZERO);
    Map<Integer, Map<String, String>> dm = DiagnosticsExtractionSupport.extractDiagnostics(e0);
    assertEquals(3, dm.size());
    assertEquals(1, dm.get(0).size());
    assertEquals(KEY_ZERO, dm.get(0).keySet().iterator().next());
    assertEquals(VALUE_ZERO, dm.get(0).values().iterator().next());
    assertEquals(1, dm.get(1).size());
    assertEquals(KEY_ONE, dm.get(1).keySet().iterator().next());
    assertEquals(VALUE_ONE, dm.get(1).values().iterator().next());
    assertEquals(1, dm.get(2).size());
    assertEquals(KEY_TWO, dm.get(2).keySet().iterator().next());
    assertEquals(VALUE_TWO, dm.get(2).values().iterator().next());
  }

  @Test
  public void testDiagnosticsWithMultipleDiagnosticsAvailableSparseCauses()
  {
    DiagnosticException e2 = new DiagnosticException().put(KEY_TWO, VALUE_TWO);
    DiagnosticException e1 = new DiagnosticException(new Exception(new Exception(e2))).put(KEY_ONE, VALUE_ONE);
    DiagnosticException e0 = new DiagnosticException(new Exception(e1)).put(KEY_ZERO, VALUE_ZERO);
    Map<Integer, Map<String, String>> dm = DiagnosticsExtractionSupport.extractDiagnostics(e0);
    assertEquals(3, dm.size());
    assertEquals(1, dm.get(0).size());
    assertEquals(KEY_ZERO, dm.get(0).keySet().iterator().next());
    assertEquals(VALUE_ZERO, dm.get(0).values().iterator().next());
    assertEquals(1, dm.get(1).size());
    assertEquals(KEY_ONE, dm.get(1).keySet().iterator().next());
    assertEquals(VALUE_ONE, dm.get(1).values().iterator().next());
    assertEquals(1, dm.get(2).size());
    assertEquals(KEY_TWO, dm.get(2).keySet().iterator().next());
    assertEquals(VALUE_TWO, dm.get(2).values().iterator().next());
  }
}
