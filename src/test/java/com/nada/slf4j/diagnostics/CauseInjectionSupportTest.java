
package com.nada.slf4j.diagnostics;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author troy.hart@gmail.com
 *
 */
public class CauseInjectionSupportTest
{
  Logger LOG = LoggerFactory.getLogger(CauseInjectionSupportTest.class);

  @Test
  public void test()
    throws Exception
  {
    String contextKey = "bigFoo";
    String contextValue = "FAP";

    DiagnosticException e = CauseInjectionSupport.wrapCause(Collections.singletonMap(contextKey, contextValue),
        ExceptionGenerator.randomException());

    //The diagnostics are as expected.
    assertEquals(1, e.getDiagnosticMapping().size());
    assertEquals(contextValue, e.getDiagnosticMapping().get(contextKey));

    //The wrapped exception will have it's stack trace replaced with the cause's 
    //stack trace.
    assertArrayEquals(e.getCause().getStackTrace(), e.getStackTrace());
  }

  @Test
  public void testRuntime()
    throws Exception
  {
    String contextKey = "bigFoo";
    String contextValue = "FAP";

    DiagnosticRuntimeException e = CauseInjectionSupport.wrapRuntimeCause(
        Collections.singletonMap(contextKey, contextValue), ExceptionGenerator.randomRuntimeException());

    //The diagnostics are as expected.
    assertEquals(1, e.getDiagnosticMapping().size());
    assertEquals(contextValue, e.getDiagnosticMapping().get(contextKey));

    //The wrapped exception will have it's stack trace replaced with the cause's 
    //stack trace.
    assertArrayEquals(e.getCause().getStackTrace(), e.getStackTrace());
  }

}
