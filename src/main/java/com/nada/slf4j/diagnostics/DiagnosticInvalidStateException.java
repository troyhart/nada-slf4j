
package com.nada.slf4j.diagnostics;

import java.util.Map;

/**
 * A {@link DiagnosticException} that implies a violation of business rules
 * related to the state of an object at the time of a given operation.
 * 
 * @author troy.hart@gmail.com
 *
 */
public class DiagnosticInvalidStateException
  extends DiagnosticException
{

  /**
   * 
   */
  public DiagnosticInvalidStateException()
  {
    super();
  }

  /**
   * @param diagnosticMapping
   * @param message
   * @param cause
   */
  public DiagnosticInvalidStateException(Map<String, String> diagnosticMapping, String message, Throwable cause)
  {
    super(diagnosticMapping, message, cause);
  }

  /**
   * @param diagnosticMapping
   * @param message
   */
  public DiagnosticInvalidStateException(Map<String, String> diagnosticMapping, String message)
  {
    super(diagnosticMapping, message);
  }

  /**
   * @param diagnosticMapping
   * @param cause
   */
  public DiagnosticInvalidStateException(Map<String, String> diagnosticMapping, Throwable cause)
  {
    super(diagnosticMapping, cause);
  }

  /**
   * @param diagnosticMapping
   */
  public DiagnosticInvalidStateException(Map<String, String> diagnosticMapping)
  {
    super(diagnosticMapping);
  }

  /**
   * @param message
   * @param cause
   */
  public DiagnosticInvalidStateException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * @param message
   */
  public DiagnosticInvalidStateException(String message)
  {
    super(message);
  }

  /**
   * @param cause
   */
  public DiagnosticInvalidStateException(Throwable cause)
  {
    super(cause);
  }

  @Override
  public DiagnosticInvalidStateException put(String key, String value)
  {
    super.put(key, value);
    return this;
  }
}
