
package com.nada.slf4j.diagnostics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Use this class to extract all the diagnostics from a given throwable, if
 * any are present.
 *
 *
 * @author troy.hart@gmail.com
 *
 */
public final class DiagnosticsExtractionSupport
{

  /**
   * <p>
   * This method extracts all the diagnostic information that is available
   * from the given <code>throwable</code> parameter. All the information
   * is wrapped into a <code>Map&lt;Integer, Map&lt;String, String&gt;&gt;</code> value.
   * The depth key is the first key value of the returned map. Each value
   * mapped to a depth key will be a mapping of diagnostic String/String
   * key/value information.
   * </p>
   *
   * <p>
   * The depth key will not represent actual cause depth, it will only
   * measure the relative depth of all {@link DiagnosticsAvailable} nodes
   * within the given throwable's cause tree.
   * </p>
   *
   * @param t an instance of a throwable.
   *
   * @return a relative depth keyed mapping of diagnostic information, or null
   * if the given throwable doesn't implement DiagnosticsAvailable, and neither
   * do any of the Throwables in the {@link Throwable#getCause() cause tree}.
   */
  public static final Map<Integer, Map<String, String>> extractDiagnostics(Throwable t)
  {
    // this value is relative to the number of DiagnosticsAvailable in the
    // cause chain of the given throwable, not precisely the depth of a given
    // cause within the chain.
    int relativeDepth = 0;
    Map<Integer, Map<String, String>> diagnosticsMap = null;
    for (DiagnosticsAvailable da : new AvailableDiagnosticsIterator(t)) {
      if (diagnosticsMap == null) {
        diagnosticsMap = new HashMap<Integer, Map<String, String>>();
      }
      diagnosticsMap.put(relativeDepth++, da.getDiagnosticMapping());
    }
    return diagnosticsMap;
  }

  private static final class AvailableDiagnosticsIterator
    implements Iterator<DiagnosticsAvailable>, Iterable<DiagnosticsAvailable>
  {
    Throwable throwable;

    private AvailableDiagnosticsIterator(Throwable throwable)
    {
      this.throwable = throwable;
    }

    @Override
    public Iterator<DiagnosticsAvailable> iterator()
    {
      return this;
    }

    /**
     * @returns true if there is a next Throwable implementing DiagnosticsAvailable.
     *
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext()
    {
      return (throwable instanceof DiagnosticsAvailable) || (getNextDiagnosticsAvailableCause() != null);
    }

    /**
     * @returns the next Throwable in the chain that implements DiagnosticsAvailable.
     *
     * @see java.util.Iterator#next()
     */
    @Override
    public DiagnosticsAvailable next()
    {
      Throwable next = throwable;
      if (throwable != null) {
        if (throwable instanceof DiagnosticsAvailable) {
          next = throwable;
          // bump the throwable to the next node.
          throwable = throwable.getCause();
        }
        else {
          Throwable cause = getNextDiagnosticsAvailableCause();
          next = cause;
          // bump the throwable to null if no more diagnostics are available,
          // otherwise bump it to the cause right after the next diagnostics
          // available cause.
          throwable = cause == null ? null : cause.getCause();
        }
      }
      return (DiagnosticsAvailable)next;
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException("remove not supported.");
    }

    /**
     * @return
     */
    private Throwable getNextDiagnosticsAvailableCause()
    {
      Throwable cause = throwable == null ? null : throwable.getCause();
      while (cause != null) {
        if (cause instanceof DiagnosticsAvailable) {
          break;
        }
        else {
          cause = cause.getCause();
        }
      }
      return cause;
    }

  }
}
