/**
 * This package supports the wrapping of exceptions that are not aware of
 * <code>DiagnosticsAvailable</code> and also supports some base classes that 
 * could be used to raise either a checked or unchecked exception with 
 * diagnostics mapping available.
 * 
 */
package com.nada.slf4j.diagnostics;