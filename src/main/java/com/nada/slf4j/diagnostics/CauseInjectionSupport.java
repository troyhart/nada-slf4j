
package com.nada.slf4j.diagnostics;

import java.util.Map;

/**
 * <p>
 * A utility class that facilitates the injection of a <code>diagnosticMapping</code>
 * into an exception {@link Exception#getCause() cause} tree. You may find it 
 * convenient to inject a <code>diagnosticMapping</code> like this when your 
 * code must throw an exception type which can not be made to implement 
 * {@link DiagnosticsAvailable}. Ideally your application code will always throw
 * exceptions which implement (or can be made to implement) this interface, but
 * injecting the diagnostics on the cause with the wrapping technique implemented
 * in this class you will ...
 * 
 * 
 * 
 * When you are Simply invoke one of
 * {@link #wrapCause(Map, Exception)} or {@link #wrapRuntimeCause(Map, RuntimeException)}
 * on an exception that you are going to pass as the cause of some new exception
 * that you are raising
 * </p>
 * 
 * <p>
 * Coupled with a logging provider that will recurse the <code>cause</code> tree
 * to render all diagnostics available, the technique of injecting a
 * <code>diagnosticMapping</code> into an exception cause tree is a cheap
 * and convenient way to provide relevant diagnostic context to a log event.
 * Ideally your application code will always throw exceptions which implement 
 * {@link DiagnosticsAvailable}. However, this may not always be the case. It
 * may be that your code can only throw some exception which you can not modify
 * and can not therefore have it implement {@link DiagnosticsAvailable}. For
 * such cases you can always include a cause tree with diagnostics available
 * in order to get the same benefit as logging an exception that directly 
 * implements <code>DiagnosticsAvailable</code>.
 * </p>
 * 
 * @author troy.hart@gmail.com
 *
 */
public class CauseInjectionSupport
{
  /**
   * 
   */
  private static final String MESSAGE = "Wrapping an exception to add diagnostic context.";
  /**
   * 
   */
  private static final String RUNTIME_MESSAGE = "Wrapping a runtime exception to add diagnostic context.";

  /**
   * @param diagnosticMapping
   * @param cause
   * 
   * @return A new instance of a {@link DiagnosticException} wrapping the given cause and including the given Map
   */
  public static final DiagnosticException wrapCause(Map<String, String> diagnosticMapping, Exception cause)
  {
    if (diagnosticMapping == null) {
      throw new IllegalArgumentException("null diagnosticMapping");
    }

    DiagnosticException de = new DiagnosticException(diagnosticMapping, MESSAGE, cause);
    de.setStackTrace(cause.getStackTrace());

    return de;
  }

  /**
   * @param diagnosticMapping
   * @param cause
   * 
   * @return A new instance of a {@link DiagnosticRuntimeException} wrapping the given cause and including the given Map
   */
  public static final DiagnosticRuntimeException wrapRuntimeCause(Map<String, String> diagnosticMapping,
                                                                   RuntimeException cause)
  {
    if (diagnosticMapping == null) {
      throw new IllegalArgumentException("null diagnosticMapping");
    }

    DiagnosticRuntimeException de = new DiagnosticRuntimeException(diagnosticMapping, RUNTIME_MESSAGE, cause);
    de.setStackTrace(cause.getStackTrace());

    return de;
  }
}
