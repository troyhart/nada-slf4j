
package com.nada.slf4j.diagnostics;

import java.util.Map;


/**
 * @author troy.hart@gmail.com
 *
 */
public class DiagnosticIllegalArgumentException
  extends DiagnosticRuntimeException
{

  /**
   * 
   */
  public DiagnosticIllegalArgumentException()
  {
    super();
  }

  /**
   * @param diagnosticMapping
   * @param message
   * @param cause
   */
  public DiagnosticIllegalArgumentException(Map<String, String> diagnosticMapping, String message, Throwable cause)
  {
    super(diagnosticMapping, message, cause);
  }

  /**
   * @param diagnosticMapping
   * @param message
   */
  public DiagnosticIllegalArgumentException(Map<String, String> diagnosticMapping, String message)
  {
    super(diagnosticMapping, message);
  }

  /**
   * @param diagnosticMapping
   * @param cause
   */
  public DiagnosticIllegalArgumentException(Map<String, String> diagnosticMapping, Throwable cause)
  {
    super(diagnosticMapping, cause);
  }

  /**
   * @param diagnosticMapping
   */
  public DiagnosticIllegalArgumentException(Map<String, String> diagnosticMapping)
  {
    super(diagnosticMapping);
  }

  /**
   * @param message
   * @param cause
   */
  public DiagnosticIllegalArgumentException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * @param message
   */
  public DiagnosticIllegalArgumentException(String message)
  {
    super(message);
  }

  /**
   * @param cause
   */
  public DiagnosticIllegalArgumentException(Throwable cause)
  {
    super(cause);
  }

  @Override
  public DiagnosticIllegalArgumentException put(String key, String value)
  {
    super.put(key, value);
    return this;
  }
}
