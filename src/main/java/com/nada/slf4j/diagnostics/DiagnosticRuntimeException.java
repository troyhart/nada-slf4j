
package com.nada.slf4j.diagnostics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author troy.hart@gmail.com
 *
 */
public class DiagnosticRuntimeException
  extends RuntimeException
  implements DiagnosticsAvailable
{
  private Map<String, String> diagnosticMapping;

  /**
   *
   */
  public DiagnosticRuntimeException()
  {
    this((Map<String, String>)null);
  }

  /**
   *
   */
  public DiagnosticRuntimeException(Throwable cause)
  {
    this((Map<String, String>)null, cause);
  }

  /**
   *
   */
  public DiagnosticRuntimeException(String message)
  {
    this((Map<String, String>)null, message);
  }

  /**
   *
   */
  public DiagnosticRuntimeException(String message, Throwable cause)
  {
    this((Map<String, String>)null, message, cause);
  }

  /**
   *
   */
  public DiagnosticRuntimeException(Map<String, String> diagnosticMapping)
  {
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
  }

  /**
   *
   */
  public DiagnosticRuntimeException(Map<String, String> diagnosticMapping, Throwable cause)
  {
    super(cause);
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
  }

  /**
   *
   */
  public DiagnosticRuntimeException(Map<String, String> diagnosticMapping, String message)
  {
    super(message);
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
  }

  /**
   *
   */
  public DiagnosticRuntimeException(Map<String, String> diagnosticMapping, String message, Throwable cause)
  {
    super(message, cause);
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
  }

  /**
   * @return
   */
  @Override
  public Map<String, String> getDiagnosticMapping()
  {
    return Collections.unmodifiableMap(diagnosticMapping);
  }

  public DiagnosticRuntimeException put(String key, String value)
  {
    diagnosticMapping.put(key, value);

    return this;
  }

}
