
package com.nada.slf4j.diagnostics;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.MDC;

/**
 * @author troy.hart@gmail.com
 *
 */
public class DiagnosticException
  extends Exception
  implements DiagnosticsAvailable
{
  private Map<String, String> diagnosticMapping;

  /**
   *
   */
  public DiagnosticException()
  {
    this((Map<String, String>)null);
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  /**
   *
   */
  public DiagnosticException(Throwable cause)
  {
    this((Map<String, String>)null, cause);
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  /**
   *
   */
  public DiagnosticException(String message)
  {
    this((Map<String, String>)null, message);
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  /**
   *
   */
  public DiagnosticException(String message, Throwable cause)
  {
    this((Map<String, String>)null, message, cause);
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  /**
   *
   */
  public DiagnosticException(Map<String, String> diagnosticMapping)
  {
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  /**
   *
   */
  public DiagnosticException(Map<String, String> diagnosticMapping, Throwable cause)
  {
    super(cause);
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  /**
   *
   */
  public DiagnosticException(Map<String, String> diagnosticMapping, String message)
  {
    super(message);
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  /**
   *
   */
  public DiagnosticException(Map<String, String> diagnosticMapping, String message, Throwable cause)
  {
    super(message, cause);
    this.diagnosticMapping = diagnosticMapping == null ? new HashMap<String, String>() : diagnosticMapping;
    initDiagnosticsWithMDC(this.diagnosticMapping);
  }

  @Override
  public Map<String, String> getDiagnosticMapping()
  {
    return diagnosticMapping;
  }

  public DiagnosticException put(String key, String value)
  {
    diagnosticMapping.put(key, value);

    return this;
  }

  static final void initDiagnosticsWithMDC(Map<String, String> diagnosticMapping)
  {
    Map<String, String> mdcCopy = MDC.getCopyOfContextMap();
    if (mdcCopy != null) {
      for (String key : mdcCopy.keySet()) {
        if (!diagnosticMapping.containsKey(key)) {
          diagnosticMapping.put(key, mdcCopy.get(key));
        }
      }
    }
  }
}
