
package com.nada.slf4j.diagnostics;

/**
 * @author troy.hart@gmail.com
 *
 */
public class DiagnosticStringUtils
{

  /**
   * <p>
   * This method offers uniform pre-processing of contextual <code>String</code>
   * data. Using this function you are guaranteed that a <code>DiagnosticIllegalArgumentException</code>
   * will be raised when the given <code>valueObject</code> is null. Further, 
   * if <code>valueObject.toString() == null</code> or <code>valueObject.toString().trim().isEmpty()</code>
   * a <code>DiagnosticIllegalArgumentException</code> will be raised. If no 
   * exception is raised then the trimmed string value is returned.
   * 
   * <p>
   * Any <code>DiagnosticIllegalArgumentException</code> thrown from this method
   * will include diagnostic information keyed by <code>fieldName</code>.
   *  
   * @param fieldName a contextual name for the <code>valueObject</code>.
   * @param valueObject an object who's <code>toString()</code> method will be
   * used to produce the return value.
   * 
   * @return a trimmed string that is never empty or null.
   * 
   * @throws DiagnosticIllegalArgumentException
   */
  public static final String fieldStringValue(String fieldName, Object valueObject)
    throws DiagnosticIllegalArgumentException
  {
    if (valueObject == null) {
      throw new DiagnosticIllegalArgumentException("Required field has null value.").put(fieldName, "NULL");
    }

    String stringVal = valueObject.toString();
    if (stringVal == null) {
      // In practice valueObject will likely be a String and this block will not even be reachable. However,
      // there is some possibility that the given valueObject will be non-null but will have an implementation
      // of toString() which returns null. This condition will catch that case and the differentiated message
      // will help with diagnosis when the case is encountered.
      throw new DiagnosticIllegalArgumentException("Required field has null string value.").put(fieldName, "NULLSTRING");
    }

    if ((stringVal = stringVal.trim()).isEmpty()) {
      throw new DiagnosticIllegalArgumentException("Required field has empty string value.").put(fieldName, "EMPTY");
    }

    return stringVal;
  }
}
