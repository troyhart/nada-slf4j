
package com.nada.slf4j.diagnostics;

import java.util.Map;

/**
 * @author troy.hart@gmail.com
 *
 */
public interface DiagnosticsAvailable
{
  Map<String, String> getDiagnosticMapping();
}
