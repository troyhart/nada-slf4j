package com.nada.slf4j;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.MDC;

/**
 * <p>Wraps slf4j's {@link MDC} with an {@link AutoCloseable auto closable},
 * instance based (i.e. non-static) API. This allows for almost brain-less use 
 * of the MDC--no need to {@link MDC#remove(String)}. Simply instantiate an 
 * instance of this class within a try-with-resources statement and then
 * {@link #put(String, String) put} context entries as you please within the
 * try block. You can manually {@link #remove(String) remove} if you need also. 
 * {@link #close()} will remove every <code>put</code> value.
 * 
 * <p>
 * You should be mindful of altering the context when you use the {@link MDC}.
 * Whenever you <code>put</code> an attribute that collides with an existing
 * attribute you will not only override it's value with whatever you 
 * <code>put</code> there, but you will clear it completely from the 
 * context when you <code>remove</code> it.
 * 
 * <p>Simple code sample...</p>
 * <pre>
 * try (MdcAutoClosable acMDC = new MdcAutoClosable()) {
 *    acMDC.put("KEY1", "val1").put("KEY2", "val2");
 *      
 *    // ...
 *    
 *    // You can remove anything you want too, from
 *    // this and the underlying org.slf4j.MDC context.
 *    acMDC.remove("WHATEVER");
 *    
 * } //acMDC.close() is automatically called
 * </pre>
 * 
 * @author troy.hart@gmail.com
 */
public final class MdcAutoClosable
  implements AutoCloseable
{
  private Set<String> keys;

  /**
   * A new instance with no context entries.
   */
  public MdcAutoClosable()
  {
  }

  /**
   * @param context initial context mapping, each entry is {@link #put(String, String)} onto 
   * this instance.
   */
  public MdcAutoClosable(Map<String, String> context)
  {
    if (context == null) {
      // should use the other constructor if not initializing from another context.
      throw new NullPointerException("Null context.");
    }

    for (Map.Entry<String, String> me : context.entrySet()) {
      put(me.getKey(), me.getValue());
    }
  }
  
  /**
   * A new instance with a single initial entry.
   * @param key initial entry key
   * @param value initial entry value
   */
  public MdcAutoClosable(String key, Object value)
  {
    this();
    put(key, value);
  }

  /**
   * {@link MDC#put(String, String) Puts} the key/value pair on the {@link MDC}, remembering the 
   * key in order to {@link MDC#remove(String) remove} from the {@link MDC} on {@link #close()}.
   * 
   * <p>
   * putting a null value is equivalent to {@link #remove(String)}.
   *  
   * @param key
   * @param value
   */
  public MdcAutoClosable put(String key, String value)
  {
    if (keys == null) {
      // not thread safe. However, this won't matter for the standard use case.
      keys = new HashSet<String>();
    }

    if (value == null) {
      remove(key);
    }
    else {
      keys.add(key);
      MDC.put(key, value);
    }

    return this;
  }

  public MdcAutoClosable put(String key, Object value)
  {
    return put(key, value == null ? null : value.toString());
  }

  /**
   * @param key a context attribute key
   * 
   * @return true if the key is defined in the local context.
   */
  public boolean isLocal(String key)
  {
    return keys.contains(key);
  }

  /**
   * Same as calling {@link MDC#get(String)}.
   * 
   * @param key
   * 
   * @return the value from {@link MDC#get(String)}
   */
  public String get(String key)
  {
    return MDC.get(key);
  }

  /**
   * {@link MDC#remove(String) Remove} the key/value pair from the {@link MDC}. Also, removed the
   * key from the instance's collection of {@link #put(String, String) put keys}.
   * 
   * @param key
   */
  public MdcAutoClosable remove(String key)
  {
    if (keys!=null) {
      keys.remove(key);
    }
    MDC.remove(key);

    return this;
  }

  /**
   * Each MDC entry {@link #put(String, String) put via this instance} (but not 
   * {@link #remove(String) removed via this instance}) will be 
   * {@link MDC#remove(String) removed from the MDC} upon invocation of this method.  
   */
  @Override
  public void close()
  {
    if (keys != null) {
      for (String key : keys) {
        MDC.remove(key);
      }
      keys = null;
    }
  }

}
