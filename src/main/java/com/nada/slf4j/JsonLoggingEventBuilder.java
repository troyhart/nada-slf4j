
package com.nada.slf4j;

import java.util.List;
import java.util.Map;

/**
 * @author troy.hart@gmail.com
 *
 */
public interface JsonLoggingEventBuilder
{
  void setApplicationName(String appName);

  void setApplicationHostName(String hostName);

  void setTimestamp(String timestamp);

  void setMessage(String message);

  void setLevel(String level);

  void setLogger(String logger);

  void setThread(String thread);

  void setStacktrace(String stacktrace);

  void setMDC(Map<String, String> mdc);

  void setDiagnostics(Map<Integer, Map<String, String>> diagnostics);

  void setMarkers(List<String> markers);

  String toJson();
}
