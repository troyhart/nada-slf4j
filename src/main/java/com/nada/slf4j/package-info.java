/**
 * This package defines a utility class to facilitate working with the 
 * {@link org.slf4j.MDC} (see {@link com.nada.slf4j.MdcAutoClosable}). 
 * Additionally, this package defines a builder interface for producing 
 * structured (JSON) log event data.
 */
package com.nada.slf4j;